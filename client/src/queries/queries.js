import { gql } from '@apollo/client';

const getAuthorsQuery = gql`
  {
    authors {
      name
      id
    }
  }
`;

const getBookQuery = gql`
  query ($id: ID) {
    book(id: $id) {
      id
      name
      genre
      author {
        id
        name
        age
        books {
          name
          id
        }
      }
    }
  }
`;

const getBooksQuery = gql`
  {
    books {
      name
      id
    }
  }
`;
//  AddBook($name: String!, $genre: String!, $authorId: ID!) - define what vars and their types we are accepting
//  addBook(name: $name, genre: $genre, authorId: $authorId) - access those vars
const addBookMutation = gql`
  mutation AddBook($name: String!, $genre: String!, $authorId: ID!) {
    addBook(name: $name, genre: $genre, authorId: $authorId) {
      name
    }
  }
`;

export { getBookQuery, getBooksQuery, getAuthorsQuery, addBookMutation };
