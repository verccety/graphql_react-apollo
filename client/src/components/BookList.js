import { useQuery } from '@apollo/client';
import { getBooksQuery } from '../queries/queries';
import BookDetails from './BookDetails';
import { useState } from 'react';

export default function BookList() {
  const [selected, setSelected] = useState(null);
  const { loading, data } = useQuery(getBooksQuery);
  if (loading) {
    return <p>Loading...</p>;
  }
  return (
    <div style={{ display: 'grid', gridTemplateColumns: '60% 40%' }}>
      <ul id='book-list'>
        {data.books.map((book) => (
          <li key={book.id} onClick={(e) => setSelected(book.id)}>
            {book.name}
          </li>
        ))}
      </ul>
      <BookDetails bookId={selected} />
    </div>
  );
}
