import { useQuery, useMutation } from '@apollo/client';
import { getAuthorsQuery, addBookMutation, getBooksQuery } from '../queries/queries';
import { useState } from 'react';

export default function AddBook() {
  const [name, setName] = useState('');
  const [genre, setGenre] = useState('');
  const [authorId, setAuthorId] = useState('');

  const getAuthors = useQuery(getAuthorsQuery);
  const [mutateFunction] = useMutation(addBookMutation);

  if (getAuthors.loading) {
    return <p>Loading Authors...</p>;
  }

  const AUTHORS_LIST = getAuthors.data.authors.map((author) => (
    <option key={author.id} value={author.id}>
      {author.name}
    </option>
  ));

  const handleSubmit = (e) => {
    e.preventDefault();

    mutateFunction({
      variables: {
        name,
        genre,
        authorId,
      },
      refetchQueries: [{query: getBooksQuery}] // update view with the latest updates after adding a new record to the DB
    });
  };
  return (
    <form id='add-book' onSubmit={handleSubmit}>
      <div className='field'>
        <label>Book name:</label>
        <input type='text' onChange={(e) => setName(e.target.value)} />
      </div>

      <div className='field'>
        <label>Genre:</label>
        <input type='text' onChange={(e) => setGenre(e.target.value)} />
      </div>

      <div className='field'>
        <label>Author:</label>
        <select onChange={(e) => setAuthorId(e.target.value)}>
          <option>--Select Author--</option>
          {[...AUTHORS_LIST]}
        </select>
      </div>

      <button>+</button>
    </form>
  );
}
