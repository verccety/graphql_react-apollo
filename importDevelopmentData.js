/* eslint-disable no-console */
const dotenv = require('dotenv');
const mongoose = require('mongoose');

const Book = require('./models/book');
const Author = require('./models/author');

dotenv.config({ path: './config.env' });

const DB = process.env.DATABASE.replace('<PASSWORD>', process.env.DATABASE_PASSWORD);
mongoose
  .connect(DB, {
    useNewUrlParser: true,
    useCreateIndex: true,
    useFindAndModify: false,
    useUnifiedTopology: true,
  })
  .then(() => {});

let books = [
  { name: 'Name of the Wind', genre: 'Fantasy' },
  { name: 'The Final Empire', genre: 'Fantasy' },
  { name: 'The Long Earth', genre: 'Sci-Fi' },
  { name: 'The Hero of Ages', genre: 'Fantasy' },
  { name: 'The Colour of Magic', genre: 'Fantasy' },
  { name: 'The Light Fantastic', genre: 'Fantasy' },
];

let authors = [
  { name: 'Patrick Rothfuss', age: 44 },
  { name: 'Brandon Sanderson', age: 42 },
  { name: 'Terry Pratchett', age: 66 },
];

// READ JSON FILE

const booksNew = books;
const authorsNew = authors;

// IMPORT DATA INTO DB

const importData = async () => {
  try {
    await Book.create(booksNew);
    await Author.create(authorsNew, { validateBeforeSave: false });
    console.log('Data successfuly loaded');
  } catch (error) {
    console.log(error);
  }
  process.exit(0);
};

// DELETE ALL DATA FROM DB

const deleteData = async () => {
  try {
    await Book.deleteMany();
    await Author.deleteMany();
    console.log('Data successfuly deleted');
  } catch (error) {
    console.log(error);
  }
  process.exit(0);
};

if (process.argv[2] === '--import') {
  importData();
} else if (process.argv[2] === '--delete') {
  deleteData();
}
